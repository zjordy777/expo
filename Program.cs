﻿using chain_of_responsibility.chain;
using System;
//nombre: ZAMBRANO SABANDO EDISSON JORDY

namespace chain_of_responsibility
{
    class Program
    {
        static void Main(string[] args)
        {
            //en esta parte definiremos lo que es la cadena

            var retiro1 = new retiro1();
            var retiro2 = new retiro2();
            var retiro3 = new retiro3();

            //definimos en que forma estan derivados el uno del otro

            retiro2.AgregarSiguiente(retiro3);
            retiro1.AgregarSiguiente(retiro2);

            //definimos variable solicitud
            
            //simulamos las operaciones

            var a = new solicitud();
            double importe = 1;
            while (importe != 0)
            {
                Console.WriteLine("Ingrese el valor de la solicitud (ingrese 0 para cancelar)");
                importe = double.Parse(Console.ReadLine());
                a.importe = importe;
                retiro1.Procesar(a);
            }
        }
    }
}
