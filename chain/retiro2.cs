﻿using System;
using System.Collections.Generic;
using System.Text;
//nombre: ZAMBRANO SABANDO EDISSON JORDY
namespace chain_of_responsibility.chain
{
    public class retiro2 : operador
    // en esta clase se realizara el segunto proceso
    {
        public override void Procesar(solicitud a)
        {
            // si el valor ingresado cumple la siguiente condicion es aprobado por el retiro numero 2
            if (a.importe <= 40)
            {
                Console.WriteLine(string.Format("solicitud aprobada por {0}", this.GetType().Name));
            }
            // caso contrario da paso al siguiente moderador de la cadena
            else
            {
                _siguiente.Procesar(a);
            }
        }
    }
}
