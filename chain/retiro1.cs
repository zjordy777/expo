﻿using System;
using System.Collections.Generic;
using System.Text;
//nombre: ZAMBRANO SABANDO EDISSON JORDY
namespace chain_of_responsibility.chain
{
    //en esta clase se realizara la operacion dependiendo el valor solicitado

    public class retiro1 : operador
    {
        //aqui procesaremos la solicitud
        public override void Procesar(solicitud a)
        {
            // si el valor ingresado cumple la siguiente condicion es aprobado por el retiro numero 1
            if (a.importe <= 20 )
            {
                Console.WriteLine(string.Format("solicitud aprobada por {0}", this.GetType().Name));
            }
            // caso contrario da paso al siguiente moderador de la cadena
            else
            {
                _siguiente.Procesar(a);
            }
        }
    }
}
