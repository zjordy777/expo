﻿using System;
using System.Collections.Generic;
using System.Text;
//nombre: ZAMBRANO SABANDO EDISSON JORDY
namespace chain_of_responsibility.chain
{
    public abstract class operador
    {
        //va a sobreescribir una operacion llamada siguiente con una instancia de operador
        protected operador _siguiente;

        public void AgregarSiguiente(operador clase_Abstract)
        {
            _siguiente = clase_Abstract;
        }
        //operacion abstracta que procesara la solicitud
        public abstract void Procesar(solicitud a);

    }
}
